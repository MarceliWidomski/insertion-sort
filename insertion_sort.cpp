//============================================================================
// Name        : insertion_sort.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
int size();
char name();
void fillArray(int size, int array[], char name = ' ');
void printArray(int size, int array[], char name = ' ');
void insertionSort(int size, int array[]);
//pytanie do Pana Filipa
//skoro dziala przekazywanie tablicy do funkcji to po co przekazywac ja jako wskaznik?
//kompilator sam konwertuje to przekazanie na wskaznik?
//czy w deklaracji i definicji musze podawac rozmiar tablicy przekazywanej do funkcji?
int main() {
	int maxSize(100);
	int array[maxSize] = { };
	char arrayName;
	arrayName = name();
	int arraySize;
	arraySize = size();
	fillArray(arraySize, array, arrayName);
	printArray(arraySize, array, arrayName);
	insertionSort(arraySize, array);
	printArray(arraySize, array, arrayName);
	return 0;
}
int size() {
	int size;
	cout << "Enter number of elements which are to be sorted: ";
	cin >> size;
	return size;
}
char name() {
	char name;
	cout << "Enter array name: " << endl;
	cin >> name;
	return name;
}
void fillArray(int size, int array[], char name) { // fills array with elements entered by user
	for (int i = 0; i < size; ++i) {
		cout << "Enter element with index " << name << " " << i << " " << ": ";
		cin >> array[i];
	}
	cout << endl;
}
void printArray(int size, int array[], char name) { // prints array
	cout << "Array " << name << " :" << endl;
	for (int i = 0; i < size; ++i)
		cout << array[i] << " ";
	cout << endl;
}
void insertionSort(int size, int array[]) {
	cout << "Sorting..." << endl;
	for (int i = size - 2; i >= 0; --i) {
		int temp(array[i]);
		int j = i + 1;
		while (temp > array[j] && j < size){
			array[j-1]=array[j];
			array[j] = temp;
			++j;
		}
	}
}
